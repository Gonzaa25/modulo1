var Bicicleta = require('../../models/bicicleta');

exports.bicicletas_list = function(req,res){
    res.status(200).json({bicicletas:Bicicleta.allBicis});
}

exports.bicicletas_delete = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(200).json({bicicletas:Bicicleta.allBicis});
}

exports.bicicletas_update = function(req,res){
    var Bici = Bicicleta.findById(req.body.id);
    Bici.color=req.body.color;
    Bici.modelo=req.body.modelo;
    Bici.ubicacion=req.body.ubicacion;
    res.status(200).json({bicicletas:Bicicleta.allBicis});
}

exports.bicicletas_create = function(req,res){
    var Bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo,req.body.ubicacion);
    Bicicleta.add(Bici);
    res.status(200).json({bicicletas:Bicicleta.allBicis});
}