//unused const { bicicletas_create_get } = require("../controllers/bicicleta");

var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id:' + this.id + ' color:' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(element => element.id == aBiciId);
    if (aBici) {
        return aBici;
    } else {
        throw new Error('La bici ya no existe!');
    }
}

Bicicleta.removeById = function (aBiciId) {
    for (i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}



var a = new Bicicleta(1, 'gris', 'playa', [-34.500908, -58.4972602]);
var b = new Bicicleta(2, 'rojo', 'urbana', [-34.510908, -58.4912602]);
var c = new Bicicleta(3, 'negro', 'playa', [-34.520908, -58.4972602]);
var d = new Bicicleta(4, 'blanco', 'urbana', [-34.530908, -58.4973602]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);

module.exports = Bicicleta;