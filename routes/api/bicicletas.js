var express = require('express');
var router = express.Router();
var bicisController = require('../../controllers/api/bicicletaControllerAPI')

/* GET home page. */
router.get('/', bicisController.bicicletas_list);
router.post('/create', bicisController.bicicletas_create);
router.post('/delete', bicisController.bicicletas_delete);
router.post('/update', bicisController.bicicletas_update);

module.exports = router;